#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Shape to Spiral - Some silly spirals from bounding boxes
# Appears under Extensions>Render>Shape To Spiral
# An Inkscape 1.2+ Extension
##############################################################################


import inkex
from inkex import PathElement, Layer, Boolean

import random
from time import time


# Function to list object attributes
####################################

def get_attributes(self):
    attribute_string = ''
    for att in dir(self):
        try:
            attribute = (att, getattr(self, att))
            attribute_string = attribute_string + str(attribute) + '\n'
        except:
            None

    return attribute_string


class ShapeToSpiral(inkex.EffectExtension):

    def add_arguments(self, pars):
        # Radius From Bbox Options
        pars.add_argument("--bbox_width_height_radius_radio", type=str, dest="bbox_width_height_radius_radio", default='width')


        # Radius Change Options
        pars.add_argument("--radius_change_cb", type=Boolean, dest="radius_change_cb", default=True)
        pars.add_argument("--radius_change_lower_float", type=float, dest="radius_change_lower_float", default=16)
        pars.add_argument("--radius_change_upper_float", type=float, dest="radius_change_upper_float", default=16)
        pars.add_argument("--radius_change_random_cb", type=Boolean, dest="radius_change_random_cb", default=True)

        # Expansion Options
        pars.add_argument("--expansion_cb", type=Boolean, dest="expansion_cb", default=True)
        pars.add_argument("--expansion_lower_float", type=float, dest="expansion_lower_float", default=16)
        pars.add_argument("--expansion_upper_float", type=float, dest="expansion_upper_float", default=16)
        pars.add_argument("--expansion_random_cb", type=Boolean, dest="expansion_random_cb", default=True)

        # Revolution Options
        pars.add_argument("--revolution_cb", type=Boolean, dest="revolution_cb", default=True)
        pars.add_argument("--revolution_lower_float", type=float, dest="revolution_lower_float", default=16)
        pars.add_argument("--revolution_upper_float", type=float, dest="revolution_upper_float", default=16)
        pars.add_argument("--revolution_random_cb", type=Boolean, dest="revolution_random_cb", default=True)

        # Argument Options
        pars.add_argument("--argument_cb", type=Boolean, dest="argument_cb", default=True)
        pars.add_argument("--argument_lower_float", type=float, dest="argument_lower_float", default=16)
        pars.add_argument("--argument_upper_float", type=float, dest="argument_upper_float", default=16)
        pars.add_argument("--argument_random_cb", type=Boolean, dest="argument_random_cb", default=True)

        # t0 Options
        pars.add_argument("--t0_cb", type=Boolean, dest="t0_cb", default=True)
        pars.add_argument("--t0_lower_float", type=float, dest="t0_lower_float", default=16)
        pars.add_argument("--t0_upper_float", type=float, dest="t0_upper_float", default=16)
        pars.add_argument("--t0_random_cb", type=Boolean, dest="t0_random_cb", default=True)


    def effect(self):
        selection_list = self.svg.selected
        if len(selection_list) < 1:
            return

        # Make Layer
        spiral_layer = Layer()
        spiral_layer.set('id', 'Spirals_' + str(time()))

        for item in selection_list:
            if item.TAG == 'text':
                inkex.errormsg(f'Cannot Spiralise Text Object ({item.get_id()}')
                continue
            else:

                ibbox = item.bounding_box()

                spiral = PathElement()
                spiral_style = {'fill': 'none', 'stroke': 'red', 'stroke-width': '0.5'}
                spiral.set('style', spiral_style)
                spiral.set('sodipodi:type', 'spiral')

                radius_source = self.options.bbox_width_height_radius_radio
                radius_value = getattr(ibbox, radius_source) / 2

                if self.options.radius_change_cb and not self.options.radius_change_random_cb:
                    radius_value = radius_value + self.options.radius_change_lower_float
                elif self.options.radius_change_cb and self.options.radius_change_random_cb:
                    radius_value = radius_value + random.uniform(self.options.radius_change_lower_float, self.options.radius_change_upper_float)

                spiral.set('sodipodi:radius', radius_value)  # Get radius from bbox half width

                spiral.set('sodipodi:cx', ibbox.center[0])
                spiral.set('sodipodi:cy', ibbox.center[1])

                if self.options.expansion_cb and not self.options.expansion_random_cb:
                    expansion_value = radius_value + self.options.expansion_lower_float
                    spiral.set('sodipodi:expansion', expansion_value)
                elif self.options.expansion_cb and self.options.expansion_random_cb:
                    expansion_value = radius_value + random.uniform(self.options.expansion_lower_float, self.options.expansion_upper_float)
                    spiral.set('sodipodi:expansion', expansion_value)

                if self.options.revolution_cb and not self.options.revolution_random_cb:
                    revolution_value = radius_value + self.options.revolution_lower_float
                    spiral.set('sodipodi:revolution', revolution_value)
                elif self.options.revolution_cb and self.options.revolution_random_cb:
                    revolution_value = radius_value + random.uniform(self.options.revolution_lower_float, self.options.revolution_upper_float)
                    spiral.set('sodipodi:revolution', revolution_value)

                if self.options.argument_cb and not self.options.argument_random_cb:
                    argument_value = radius_value + self.options.argument_lower_float
                    spiral.set('sodipodi:argument', argument_value)
                elif self.options.argument_cb and self.options.argument_random_cb:
                    argument_value = radius_value + random.uniform(self.options.argument_lower_float, self.options.argument_upper_float)
                    spiral.set('sodipodi:argument', argument_value)

                if self.options.t0_cb and not self.options.t0_random_cb:
                    t0_value = radius_value + self.options.t0_lower_float
                    spiral.set('sodipodi:t0', t0_value)
                elif self.options.t0_cb and self.options.t0_random_cb:
                    t0_value = radius_value + random.uniform(self.options.t0_lower_float, self.options.t0_upper_float)
                    spiral.set('sodipodi:t0', t0_value)


                spiral.transform = spiral.transform @ item.composed_transform()

                spiral_layer.append(spiral)

        if len(spiral_layer.getchildren()) > 0:
            self.svg.append(spiral_layer)

if __name__ == '__main__':
    ShapeToSpiral().run()
